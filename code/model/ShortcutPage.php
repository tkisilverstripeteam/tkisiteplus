<?php

class ShortcutPage extends RedirectorPage {

	private static $db = array(
        'ShortcutMode' => "Enum('SelectedPage,FirstSubPage,RandomSubPage,Parent','SelectedPage')"
	);

	private static $defaults = array(
		"RedirectionType" => "Internal"
	);

    public function redirectionLink() {
		if($this->RedirectionType == 'External') {
			if($this->ExternalURL) {
				return $this->ExternalURL;
			}
		} else {
            $linkPage = null;
            // Tmp: @todo add CMS fields for shortcut modes?
            if($this->LinkToID) {
                $linkPage = $this->LinkToID ? DataObject::get_by_id("SiteTree", $this->LinkToID) : null;
            } else {
                switch($this->ShortcutMode) {
                    case 'SelectedPage':
                        $linkPage = $this->LinkToID ? DataObject::get_by_id("SiteTree", $this->LinkToID) : null;
                        break;
                    case 'FirstSubPage':
                        $linkPage = $this->Children()->first();
                        break;
                    case 'RandomSubPage':
                        //@todo
                        break;
                    case 'Parent':
                        $linkPage = $this->Parent();
                        break;
                }
            }
            
		
			if($linkPage) {
				// We shouldn't point to ourselves - that would create an infinite loop!  Return null since we have a
				// bad configuration
				if($this->ID == $linkPage->ID) {
					return null;
			
				// If we're linking to another redirectorpage then just return the URLSegment, to prevent a cycle of redirector
				// pages from causing an infinite loop.  Instead, they will cause a 30x redirection loop in the browser, but
				// this can be handled sufficiently gracefully by the browser.
				} elseif($linkPage instanceof RedirectorPage) {
					return $linkPage->regularLink();

				// For all other pages, just return the link of the page.
				} else {
					return $linkPage->Link();
				}
			}
		}
	}
    
}
class ShortcutPage_Controller extends RedirectorPage_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

}
