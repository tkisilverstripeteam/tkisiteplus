<?php

class ExternalLinkPage extends RedirectorPage {

	private static $defaults = array(
		"RedirectionType" => "External"
	);

	public function onBeforeWrite() {
		Page::onBeforeWrite();
        
		if ($this->ExternalURL && substr($this->ExternalURL, 0, 2) !== '//') {
			$urlParts = parse_url($this->ExternalURL);
			if ($urlParts) {
				if (empty($urlParts['scheme'])) {
					// no scheme, assume http
					$this->ExternalURL = 'http://' . $this->ExternalURL;
				} elseif (!in_array($urlParts['scheme'], array(
					'http',
					'https',
					'ftp',
					'mailto'
				))) {
					// we only allow above schemes
					$this->ExternalURL = '';
				}
			} else {
				// malformed URL to reject
				$this->ExternalURL = '';
			}
		}
	}
    
    public function AbsoluteLink($action = null) {
		if($link = $this->redirectionLink()) return $link;
		else return $this->regularLink();
	}

}

class ExternalLinkPage_Controller extends RedirectorPage_Controller {

	/**
	 * An array of actions that can be accessed via a request. Each array element should be an action name, and the
	 * permissions or conditions required to allow the user to access it.
	 *
	 * <code>
	 * array (
	 *     'action', // anyone can access this action
	 *     'action' => true, // same as above
	 *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
	 *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
	 * );
	 * </code>
	 *
	 * @var array
	 */
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		// You can include any CSS or JS required by your project here.
		// See: http://doc.silverstripe.org/framework/en/reference/requirements
	}

}
