<?php

class TkiSitePlusDisabledExtension extends DataExtension {

    private static $db = array(
        'Disabled' => 'Boolean'
    );
    
    public function updateCMSFields(FieldList $fields)
    {
        // Remove legacy data fields
        $disabledField = $fields->dataFieldByName('Disabled');
        $fields->removeByName(['Disabled']);

        $fields->findOrMakeTab('Root.Visibility',_t('TkiSitePlusDisabledExtension.Visibility', 'Visibility'));
        $fields->addFieldsToTab('Root.Visibility',CheckboxField::create('Disabled',_t('TkiSitePlusDisabledExtension.Disabled','Disabled')));
        
    }
    
    /**
     * Update any requests to limit the results to the current site
     * @todo test
     */
    public function augmentSQL(SQLQuery &$query)
    {
        if(Versioned::current_stage() == 'Live') {
            foreach ($query->getFrom() as $tableName => $info) {
                $where = "\"$tableName\".\"Disabled\" != 1";
                $query->addWhere($where);
                break;
            }
        }
    }
}
