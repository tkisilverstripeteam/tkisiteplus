<?php

/** 
 * Deprecated
 */
class TkiSitePlusAuthoredExtension extends DataExtension {

    private static $db = array(
        'SubTitle' => "Varchar(255)",
        'Description' => 'Text',
        'Abstract' => 'Text',             // Abstract of content
        'AuthorName' => 'Varchar',        // Author name, if not associated with a member via Author
        'AuthorEmail' => 'Varchar',       // Author email, if not associated with a member via Author
        'DateUpdated' => 'SS_Datetime'    // Date content was last updated
    );

    private static $has_one = array(
        'Creator' => 'Member',
        'Author' => 'Member'
    );

    public function updateCMSFields(FieldList $fields)
    {
        $fields->findOrMakeTab('Root.Metadata',_t('TkiSitePlusAuthoredExtension.MetaTab', 'Metadata'));
        $author = $this->owner->Author();

        if($this->owner instanceof SiteTree) {
            $metaFieldDesc = $fields->dataFieldByName('MetaDescription');
            $metaFieldExtra = $fields->dataFieldByName('ExtraMeta');
            $fields->removeByName('MetaDescription');
            $fields->removeByName('ExtraMeta');
            $fields->removeByName('Metadata');
            $fields->addFieldsToTab('Root.Metadata',[
                 // Meta tag fields
                HeaderField::create('MetaTagsHeading',_t('TkiSitePlusAuthoredExtension.MetaTagsHeading','Meta tags'),4),
                CompositeField::create(array($metaFieldDesc,$metaFieldExtra)),
            ]);
        }
        
        // Editorial fields
        $fields->addFieldToTab('Root.Metadata',
            HeaderField::create('EditorialHeading',_t('TkiSitePlusAuthoredExtension.EditorialHeading','Editorial'),4)
        );
        $dateField = DatetimeField::create('DateUpdated',_t('TkiSitePlusAuthoredExtension.DateUpdated','Last updated'));
        $dateField->getDateField()->setConfig('showcalendar', true);
        $fields->addFieldToTab('Root.Metadata',$dateField);
        
        /*
         * Author
         */
        // Use all back end users as potential authors
        $filter = array('Code' => 'back-end');
        $backendGroup = Group::get()->filter($filter)->first();
        $authorOptions = ($backendGroup) ? $backendGroup->Members()->map()->toArray() : array();
        $authorOptions['-1'] = _t('TkiSitePlusAuthoredExtension.Other','Other');
        $authorField = DropdownField::create('AuthorID',_t('TkiSitePlusAuthoredExtension.Author','Author'),$authorOptions);
        $authorField->setHasEmptyDefault(true);
        
        $otherAuthorFields = DisplayLogicWrapper::create(array(
            TextField::create('AuthorName',_t('TkiSitePlusAuthoredExtension.AuthorName','Author name')),
            EmailField::create('AuthorEmail',_t('TkiSitePlusAuthoredExtension.AuthorEmail','Author email'))
        ));
        $otherAuthorFields->displayIf('AuthorID')->isEqualTo('-1'); 

        $fields->addFieldsToTab('Root.Metadata',[   
            $authorField,
            $otherAuthorFields,
            TextAreaField::create('Abstract',_t('TkiSitePlusAuthoredExtension.Abstract','Abstract'))->setRows(5),
        ]);       
       
        
    }
    
    public function onBeforeWrite()
    {
        // Other auth
        if($this->owner->AuthorID !== '-1') {
            $this->owner->AuthorName = null;
            $this->owner->AuthorEmail = null;
        }
	}
    
}
