<?php

/**
 * Adds fields compatible with T3 fe_users
 * @deprecated. Can use tkicontactinfo if needed for future front end users
 */
class TkiSitePlusMemberExtension extends DataExtension {

    private static $db = array(
        'Description' => 'Text',
        'Title' => 'Varchar(40)',       // Prefix
        'Address' => 'Varchar(255)',    // Street address
        'City' => 'Varchar(50)',
        'Country' => 'Varchar(40)',
        'Postcode' => 'Varchar(10)',
        'Telephone' => 'Varchar(20)',
        'Fax' => 'Varchar(20)',
        'Website' => 'Varchar(80)',
        'Company' => 'Varchar(80)'
    );

    private static $has_one = array(
        'Creator' => 'Member',
        'Image' => 'Image'
    );

}
