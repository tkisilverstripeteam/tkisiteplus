<?php

class TkiSitePlusGroupExtension extends DataExtension 
{

  private static $default_records = array(
      array('Title' => "Back End", 'Code' => 'back-end', 'Description' => 'Base back end group'),
      array('Title' => "Front End", 'Code' => 'front-end', 'Description' => 'Base front end group'),
  );

 
  /**
   * Adds default groups
   */
  public function requireDefaultRecords() {
		$defaultRecords = $this->owner->stat('default_records');
        
		if(!empty($defaultRecords) && is_array($defaultRecords)) {
            foreach($defaultRecords as $record) {
              $className = $this->owner->class;

              // Check for existing record
              $res = DataModel::inst()->$className
                    ->filter(array('Code' => $record['Code']))
                    ->first();
              if($res) {
                  continue;
              }
              
              $obj = DataModel::inst()->$className->newObject($record);
              $obj->write();
            }
         DB::alteration_message("Added default records to $className table","created");
        }
	}
}
